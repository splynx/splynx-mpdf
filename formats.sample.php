<?php

/**
 * Function to get mPDF config by type.
 * For params description see: https://mpdf.github.io/reference/mpdf-functions/mpdf.html
 *
 * Copy this file to `formats.php` to set your formats
 *
 * @param string $type Type of document
 * @return array
 */
function get_mPDF_config($type)
{
    // Default config
    // You can copy this config array to `switch` to those types
    // That you want to change format
    $config = [
        'mode' => '',
        'format' => 'A4',
        'default_font_size' => 0,
        'default_font' => '',
        'margin_left' => 15,
        'margin_right' => 15,
        'margin_top' => 16,
        'margin_bottom' => 16,
        'margin_header' => 9,
        'margin_footer' => 9,
        'orientation' => 'P'
    ];

    switch ($type) {
        // Invoices
        case 'invoices':
        case 'invoices_one':
        case 'invoices_all':
            break;

        // Payments
        case 'payments':
        case 'payments_one':
        case 'payments_all':
            break;

        // Requests
        case 'requests':
        case 'requests_one':
        case 'requests_all':
            break;

        // Customer document
        case 'customer_documents':
            break;
    }

    // Here you can declare some callback function to change HTML code
    /**
     * @param string $html Html code
     * @param array $properties PDF properties
     * @param boolean $isExport True if this is exporting process
     * @return mixed
     */
/*
    $config['callback'] = function ($html, $properties, $isExport) {
        // Make some changes in HTML
        if (!$isExport) {
            $html = str_replace('foo', 'bar', $html);
        } else {
            $html = str_replace('foo', 'bar exported', $html);
        }
        return $html;
    };
*/

    return $config;
}
