<?php

$packageDescription = 'mPDF add-on for Splynx';

$controlConfig = [
    'Conflicts' => 'splynx-mpdf-addon, splynx-mpdf-dev',
    'Replaces' => 'splynx-mpdf-addon',
    'Depends' => 'php8.2-common, php8.2-bcmath',
];

$currentReleaseVersion = '8.2.4';
