Splynx mPdf add-on
==================

REQUIREMENTS
------------
The minimum requirement that your Web server supports PHP 8.2


INSTALLATION
------------

### Install from an Archive File
Extract the archive file downloaded from [here](https://bitbucket.org/splynx/splynx-mpdf/downloads/) to a directory `addons` in your Splynx dir. Rename extracted folder to `splynx-mpdf`.

### Install via GIT
Install [GIT](https://git-scm.com) then install this project using the following command:

~~~
cd /var/www/splynx/addons
git clone https://bitbucket.org/splynx/splynx-mpdf.git
cd splynx-mpdf
composer install
~~~

When you want update add-on run following command:
~~~
git pull
composer update
~~~

### Install via Composer

In this type of installation you can edit source files as you want.
Install Composer by following the instructions at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).
Then install this project using the following command:

~~~
cd /var/www/splynx/addons
php composer.phar create-project --prefer-dist splynx/splynx-mpdf splynx-mpdf
~~~
