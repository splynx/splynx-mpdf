<?php

/**
 * This file is used to set some mPdf params.
 * Don't edit this file! It will be overwritten in when update!
 * If you want to use your params copy this file to file "mpdf_params.php" and edit it.
 */

return [
    'setAutoTopMargin' => 'stretch',
    'setAutoBottomMargin' => 'stretch',
    'use_kwt' => true,
    'autoScriptToLang' => false,
    'autoLangToFont' => false
];
