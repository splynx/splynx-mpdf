<?php

function getMPDFInstance($mPDFConfig)
{
    if (file_exists(__DIR__ . '/config/mpdf_params.php')) {
        /** @var array $mPdfParams */
        $mPdfParams = require(__DIR__ . '/config/mpdf_params.php');
    } else {
        /** @var array $mPdfParams */
        $mPdfParams = require(__DIR__ . '/config/mpdf_params.example.php');
    }

    return new Mpdf\Mpdf(array_merge($mPDFConfig, $mPdfParams));
}
