<?php // phpcs:disable

$baseDir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;

$configIniFile = $baseDir . 'config' . DIRECTORY_SEPARATOR . 'config.php';
$config = parse_ini_file($configIniFile, true);
date_default_timezone_set($config['splynx']['timezone']);

error_reporting(E_ERROR);

function addIteratorForFilename($filePath)
{
    $fileInfo = pathinfo($filePath);

    $path = $fileInfo['dirname'];
    $filename = $fileInfo['filename'];
    $extension = $fileInfo['extension'];

    $i = 1;

    $fullFileName = $path . DIRECTORY_SEPARATOR . $filename . '-' . $i . '.' . $extension;

    while (file_exists($fullFileName)) {
        $i++;
        $fullFileName = $path . DIRECTORY_SEPARATOR . $filename . '-' . $i . '.' . $extension;
    }

    return $fullFileName;
}
